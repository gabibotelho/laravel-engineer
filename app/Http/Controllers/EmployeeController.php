<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessQueues;
use App\Models\Employee;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use League\Csv\Reader;
use Symfony\Component\HttpKernel\Exception\HttpException;

class EmployeeController extends Controller
{
    public function get(): JsonResponse
    {
        return response()->json(
            Employee::where('user_id', auth()->user()->id)->get()
        );
    }

    public function post(Request $request): JsonResponse
    {
        $file = $request->file('file');

        $csv = Reader::createFromFileObject($file->openFile());

        $csv->setHeaderOffset(0);

        $header = $csv->getHeader(); //returns the CSV header record
        $records = $csv->getRecords(); //returns all the CSV records as an Iterator object

        $records = iterator_to_array($records);

        foreach($records as $record){
            $start_date = $record["start_date"];
            $start_date = explode("-", $start_date);
            $check_date = checkdate($start_date[1], $start_date[2], $start_date[0]);

            if($check_date == 0){
                throw new HttpException(406, 'NOT ACCEPTABLE: Invalid Date');
            }
        }

        dispatch(new ProcessQueues(auth()->user()->id, $records)); 

        return response()->json([
            "Sucess" => true,
        ]);
    }

    public function show(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json(
            $employee
        );
    }

    public function destroy(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json([
            'Success' => $employee->delete()
        ]);
    }
}
