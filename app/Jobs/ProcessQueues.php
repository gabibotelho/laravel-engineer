<?php

namespace App\Jobs;

use App\Mail\UpdateEmployeeMail;
use App\Models\Employee;
use App\Models\User;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class ProcessQueues implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $records = [];
    private $userId;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($userId, $records)
    {
        $this->userId = $userId;
        $this->records = $records;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = User::where('id', $this->userId)->first();

        foreach($this->records as $record){
            $employee = Employee::where('document', $record['document'])->first();
            if(!$employee) $employee = new Employee();

            $employee->user_id = $user->id;
            $employee->name = $record['name'];
            $employee->email = $record['email'];
            $employee->document = $record['document'];
            $employee->city = $record['city'];
            $employee->state = $record['state'];
            $employee->start_date = $record['start_date'];

            $employee->save();
        }

        try{
            Mail::to($user->email)->send(new UpdateEmployeeMail($user));
        } catch (Exception $e){

        }
        
    }
}
